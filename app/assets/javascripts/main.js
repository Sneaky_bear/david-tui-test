$(document).ready(function () {
    $('.step-button').click(function () {
        var number = this.id.toString().replace('baby-step-', '');
        $('.slide-accordion').not('#step-' + number + '-accordion').slideUp();
        $('#step-' + number + '-accordion').slideDown();
    });
});